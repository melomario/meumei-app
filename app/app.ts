import {App, IonicApp, Platform, NavController} from 'ionic-angular';
import {DashboardPage} from './pages/dashboard/dashboard';
import {HomePage} from './pages/home/home';
import {MeiPage} from './pages/mei/mei';
import {JobSearchPage} from './pages/job_search/job_search';
import {JobsPage} from './pages/jobs/jobs';
import {JobCreatePage} from './pages/job_create/job_create';
import {JobTypePage} from './pages/job_type/job_type';
import {ChatPage} from './pages/chat/chat';
import {CandidatesPage} from './pages/candidates/candidates';
import {ProfilePage} from './pages/profile/profile';
import {UserService} from './providers/user-service';
import {StatusBar} from 'ionic-native';
import {InAppBrowser} from 'ionic-native';
import {UserModel} from './models/user-model'
import {LoadingModal} from './components/loading-modal/loading-modal';
import {Config} from './config';

@App({
  providers: [UserService],
  directives: [LoadingModal],
  templateUrl: 'build/app.html'
})
class MyApp {
  rootPage: any = HomePage;
  //rootPage: any = JobsPage;

  pages: Array<{icon: string, title: string, component: any}>
  user: any;


  constructor(private app: IonicApp, private platform: Platform, private userService: UserService) {
    this.initializeApp();
    this.user = UserService.current_user;
    UserService.app = this;
    console.log("Servidor de aplicação em: " + Config.SERVER);

    this.pages = [
      { icon: 'home', title: 'Home', component: DashboardPage },
      { icon: 'person', title: 'Meu Perfil', component: ProfilePage },
      { icon: 'hammer', title: 'Meus Serviços', component: JobsPage }
    ];

    console.log('app.ts - inovapps_user: ');
    console.dir(this.user);
    if(this.user && this.user.isMei){
      this.pages.push({ icon: 'search', title: 'Buscar Novos Serviços', component: JobSearchPage });
    }
  }

  refreshUser(){
    this.user = UserService.current_user;
  }
  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.overlaysWebView(false);
      StatusBar.backgroundColorByHexString('#455A64');
      // The platform is now ready. Note: if this callback fails to fire, follow
      // the Troubleshooting guide for a number of possible solutions:
      //
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //
      // First, let's hide the keyboard accessory bar (only works natively) since
      // that's a better default:
      //
      // Keyboard.setAccessoryBarVisible(false);
      //
      // For example, we might change the StatusBar color. This one below is
      // good for dark backgrounds and light text:
      // StatusBar.setStyle(StatusBar.LIGHT_CONTENT)
    });
  }

  setUser(user){
    this.user = user;
  }

  loginDone(isMei){
    if(isMei){
      //this.pages.push({ icon: 'search', title: 'Buscar Novos Serviços', component: JobSearchPage });
    }
  }


  logout(){
    this.userService.logout();
    let nav = this.app.getComponent('nav');
     nav.setRoot(HomePage);
  }
}
