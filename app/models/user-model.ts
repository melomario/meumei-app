import {IRestModel} from './rest-model-interface';

export class UserModel implements IRestModel{
  public _id: string;
  public email: string;
  public photo_url: string;
  public name: string;
  public isValidated: string;
  public compentences: Array<any>;
  public location: Array<number>;
  public birthday: string;
  public bio: string;

  constructor(public facebook_id, public accessToken, public isMei){

  }

}
