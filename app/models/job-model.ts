import {IRestModel} from './rest-model-interface';

export class JobModel implements IRestModel{
  constructor(){
    this.date = new Date()
    this.rating = -1;
  }
  public _id: string;
  public title: string;
  public description: string;
  public location: Array<number>;
  public cnae_code: string;
  public cnae_name: string;
  public status: string;
  public date: Date;
  public creator_id: string;
  public creator_name: string;
  public creator_photo: string;
  public responsible_id: string;
  public responsible_name: string;
  public responsible_photo: string;
  public rating: number;

}
