import {IRestModel} from './rest-model-interface';

export class CnaeModel implements IRestModel{
  public _id: string;
  public code: string;
  public name: string;
}
