import {Injectable} from 'angular2/core';
import {JobModel} from '../models/job-model';
import {UserModel} from '../models/user-model';
import {RESTService} from './rest-service';
import {UserService} from './user-service';
import {Http} from 'angular2/http';

@Injectable()
export class JobSearchService extends RESTService<JobModel>{
  getRouteName(): string{
    return "job-search";
  }

  constructor(http: Http){
    super(http);
  }

  list(): Promise<JobModel[]>{
    var promiseResult;
    var promiseError;
    let model = {user: UserService.current_user._id};
    let route = '/creator';
    if(UserService.current_user.isMei)
      route = '/responsible';
    console.log("inovapps_rest_list: " + this.getRouteName());
    let promise = new Promise<JobModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+route, JSON.stringify(model), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{
        promiseError(error);
      }
    );

    return promise;
  }

  listCandidates(job): Promise<UserModel[]>{
    var promiseResult;
    var promiseError;
    let model = {job: job};
    let route = '/candidates';
    console.log("inovapps_rest_list: " + this.getRouteName());
    let promise = new Promise<UserModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+route, JSON.stringify(model), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{
        promiseError(error);
      }
    );

    return promise;
  }

  refuseCandidate(job, candidate): Promise<UserModel[]>{
    var promiseResult;
    var promiseError;
    let model = {job: job._id, candidate: candidate._id};
    let route = '/refuse-candidate';
    console.log("inovapps_rest_list: " + this.getRouteName());
    let promise = new Promise<UserModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+route, JSON.stringify(model), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{
        promiseError(error);
      }
    );

    return promise;
  }

  acceptCandidate(job, candidate): Promise<UserModel[]>{
    var promiseResult;
    var promiseError;
    let model = {job: job._id, candidate: candidate._id};
    let route = '/accept-candidate';
    console.log("inovapps_rest_list: " + this.getRouteName() + route);
    let promise = new Promise<UserModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+route, JSON.stringify(model), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{
        promiseError(error);
      }
    );

    return promise;
  }

  loadFeedbacks(user_id){
    var promiseResult;
    var promiseError;
    let model = {user: user_id};
    let route = '/feedback';
    console.log("inovapps_rest_list: " + this.getRouteName() + route);
    let promise = new Promise<JobModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+route, JSON.stringify(model), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{
        promiseError(error);
      }
    );

    return promise;
  }
}
