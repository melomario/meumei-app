import {Injectable} from 'angular2/core';
import {JobModel} from '../models/job-model';
import {RESTService} from './rest-service';
import {UserService} from './user-service';
import {Http} from 'angular2/http';

@Injectable()
export class NearbyService extends RESTService<JobModel>{
  getRouteName(): string{
    return "nearby";
  }

  constructor(http: Http){
    super(http);
  }

  listNearby(): Promise<any>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_rest_list: " + this.getRouteName());
    var query = {
      user: UserService.current_user._id,
      lat:  UserService.current_user.location[1],
      lng:  UserService.current_user.location[0]
    }

    let promise = new Promise<JobModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+'/jobs/3000', JSON.stringify(query), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }

  acceptJob(job): Promise<any>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_accept_job: " + this.getRouteName());
    var query = {
      user: UserService.current_user._id,
      job:  job._id
    }

    let promise = new Promise<JobModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+'/accept', JSON.stringify(query), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }

  refuseJob(job): Promise<any>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_refuse_job: " + this.getRouteName());
    var query = {
      user: UserService.current_user._id,
      job:  job._id
    }

    let promise = new Promise<JobModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl+'/refuse', JSON.stringify(query), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }
}
