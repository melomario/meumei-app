import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import {UserModel} from '../models/user-model';
import {Config} from '../config';
import {UserService} from './user-service';

import 'rxjs/add/operator/map';

@Injectable()
export class MeiService{
  meiInfo;
  constructor(private http: Http){

  }

  getCaptcha(): Promise<any>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_mei: Obtendo Captcha");
    let promise = new Promise<any>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.get(Config.SERVER + '/api/mei/captcha')
      .map(res => res.json())
      .subscribe(
        data => {
          this.meiInfo = {
            captchaToken: data.id,
            sessionId: data.session,
            faces_id: data.faces_id
          };
          promiseResult(data);
        },
        error => {promiseError(error); console.log("inovapps_mei: " + error);},
        () => {console.log("inovapps_mei: Terminou o chamado do Captcha!");});

    return promise;
  }

  getCompetences(cpf, birthday, captcha): Promise<any>{
    this.meiInfo.cpf = cpf;
    this.meiInfo.birthday = birthday;
    this.meiInfo.captcha = captcha;
    this.meiInfo.user = UserService.current_user._id;
    console.log("inovapps_mei Validate args: " + JSON.stringify(this.meiInfo));
    var postHeader = new Headers();
    postHeader.append('Content-Type', 'application/json');
    var promiseResult;
    var promiseError;
    let promise = new Promise<any>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(Config.SERVER + '/api/mei/validate', JSON.stringify(this.meiInfo), {headers: postHeader})
    .map(res => res.json())
    .subscribe(
      data => {
        console.log("inovapps_mei Validate: " + JSON.stringify(data));
        promiseResult(data);
      },
      error => {promiseError(error); console.log("inovapps_mei Validate Error: " + JSON.stringify(error));},
      () => {console.log("inovapps_mei Validate: Terminou o chamado do Captcha!");});;

    return promise;
  }
}
