import {Http, Headers} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import {Alert} from 'ionic-angular';
import {Config} from '../config';
import 'rxjs/add/operator/map';

export abstract class RESTService<IRestModel>{
  abstract getRouteName(): string;

  baseUrl = Config.SERVER + '/api/';
  postHeader = new Headers();

  constructor(public http: Http){
    this.baseUrl += this.getRouteName();
    this.postHeader.append('Content-Type', 'application/json');
  }

  list(): Promise<IRestModel[]>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_rest_list: " + this.getRouteName());
    let promise = new Promise<IRestModel[]>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.get(this.baseUrl).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }

  save(model: IRestModel): Promise<IRestModel>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_rest_create: " + this.getRouteName());
    console.log("Dados enviados: ");
    console.log(JSON.stringify(model));
    let promise = new Promise<IRestModel>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.post(this.baseUrl, JSON.stringify(model), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }

  get(id): Promise<IRestModel>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_rest_get: " + this.getRouteName());
    let promise = new Promise<IRestModel>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.get(this.baseUrl+"/"+id).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }

  delete(id): Promise<IRestModel>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_rest_delete: " + this.getRouteName());
    let promise = new Promise<IRestModel>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.delete(this.baseUrl+"/"+id).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }

  update(model: any): Promise<IRestModel>{
    var promiseResult;
    var promiseError;
    console.log("inovapps_rest_update: " + this.getRouteName());
    let promise = new Promise<IRestModel>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    this.http.put(this.baseUrl+"/"+model._id, JSON.stringify(model), {headers: this.postHeader}).map(res => res.json())
    .subscribe(
      data => {
        promiseResult(data);
      },
      error =>{

        promiseError(error);
      }
    );

    return promise;
  }
}
