import {Injectable, Pipe} from 'angular2/core';

/*
  Generated class for the FilterPipel pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'toDate'
})
@Injectable()
export class DatePipe {
  /*
    Takes a value and makes it lowercase.
   */
  transform(date_string, [mask]) {
    let date = new Date(date_string);
    return date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear();
  }
}
