import {Page, NavController, NavParams, Alert, IonicApp} from 'ionic-angular';
import {HomePage} from '../home/home';
import {DashboardPage} from '../dashboard/dashboard';
import {UserService} from '../../providers/user-service';
import {MeiService} from '../../providers/mei-service';
import {DatePicker} from 'ionic-native';

@Page({
  providers: [UserService, MeiService],
  templateUrl: 'build/pages/mei/mei.html'
})
export class MeiPage {
  user; captcha = {}; captchaAnswer = ""; cpf; day; month; year; atividades = [];

  constructor(private nav: NavController, private userService: UserService, private meiService: MeiService, private app: IonicApp){
    this.user = UserService.current_user;
    userService.updateUserLocation();
    if(this.user.isMei && !this.user.isValidated){
      this.loadCaptcha();
    }
  }

  loadCaptcha(){
    this.app.getComponent('loading').show();
    this.meiService.getCaptcha().then( (data) => {
      this.captcha = data;
      this.app.getComponent('loading').hide();
    });
  }

  formatCpf(){
    let cpf = "";
    cpf += this.cpf.toString();

    while(cpf.length < 11)
      cpf = '0' + cpf;

    return cpf.substr(0,3) + '.' + cpf.substr(3,3) + '.' + cpf.substr(6,3) + '-' + cpf.substr(9,2);
  }

  validateMei(){
    console.log("Validando o CPF");
    let formatted_cpf = this.formatCpf();
    this.app.getComponent('loading').show();
    this.meiService.getCompetences(formatted_cpf, this.day+'/'+this.month+'/'+this.year, this.captchaAnswer).then( (data) => {
      this.app.getComponent('loading').hide();
      if(data.success){
        console.log(data);
        this.user.isValidated = true;
        this.atividades = data.atividades;
        this.user.compentences = data.atividades;
        console.log (this.user);
        UserService.current_user = this.user;
        console.log (UserService.current_user);
        this.userService.saveUser( () => this.nav.setRoot(DashboardPage));
      }
      else{
        let alert = Alert.create({
          title: 'Dados incorretos',
          subTitle: 'Não foi possível validar o seu MEI. Por favor, certifique-se de que preencheu todos os dados corretamente.',
          buttons: ['OK']
        });
        this.nav.present(alert);
      }
    });
  }
}
