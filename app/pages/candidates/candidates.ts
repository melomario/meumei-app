import {Page, NavParams, IonicApp, NavController} from 'ionic-angular';
import {UserService} from '../../providers/user-service';
import {JobSearchService} from '../../providers/job-search-service';
import {ChatPage} from '../chat/chat';
import {DatePipe} from '../../pipes/date-pipe';

@Page({
  providers: [JobSearchService],
  pipes: [DatePipe],
  templateUrl: 'build/pages/candidates/candidates.html'
})
export class CandidatesPage {
  user; job;
  candidates = [];
  feedbacks = [];
  current_candidate;

  constructor(private jobSearchService: JobSearchService, private nav: NavController, private params: NavParams, private app: IonicApp){
    this.user = UserService.current_user;
    this.job = params.get('job');
    this.loadCandidates();
  }

  loadCandidates(){
    this.app.getComponent("loading").show();
    this.jobSearchService.listCandidates(this.job._id).then((candidates) => {
      this.candidates = candidates;
      this.current_candidate = candidates[0];
      this.app.getComponent("loading").hide();
      this.updateFeedback();
    });
  }

  acceptCandidate(){
    this.jobSearchService.acceptCandidate(this.job, this.current_candidate).then((data) => {
      console.log("Serviço aceito");
      this.nav.push(ChatPage, {job: this.job});
    });
  }

  refuseCandidate(){
    this.jobSearchService.refuseCandidate(this.job, this.current_candidate).then((data) => {
      console.log("Candidato recusado");
      this.candidates.splice(0,1);
      this.current_candidate = this.candidates[0];
      this.updateFeedback();
    });
  }

  updateFeedback(){
    this.jobSearchService.loadFeedbacks(this.current_candidate._id).then((jobList) =>{
      this.feedbacks = jobList;
    });
  }
}
