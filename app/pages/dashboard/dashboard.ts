import {Page, NavController, NavParams, Alert, IonicApp} from 'ionic-angular';
import {HomePage} from '../home/home';
import {MeiPage} from '../mei/mei';
import {UserService} from '../../providers/user-service';
import {MeiService} from '../../providers/mei-service';
import {DashboardService} from '../../providers/dashboard-service';
import {DatePicker} from 'ionic-native';
import {JobSearchPage} from '../job_search/job_search';
import {JobTypePage} from '../job_type/job_type';

@Page({
  providers: [UserService, DashboardService],
  templateUrl: 'build/pages/dashboard/dashboard.html'
})
export class DashboardPage {
  data = {}; user = {};

  constructor(private nav: NavController, private dashboardService: DashboardService, private userService: UserService, private app: IonicApp){
    console.log("meumei: Validando usuário");
    if(!userService.isLoggedIn()){
      console.log("meumei: Usuário não está logado!");
      this.nav.push(HomePage);
    }
    else if(userService.isLoggedIn() && UserService.current_user.isMei && !UserService.current_user.isValidated){
      console.log("meumei: Usuário está logado e é um mei, mas não está validado!");
      this.nav.push(MeiPage);
    }
    console.log("meumei: Usuário pronto para o Dashboard");
    this.user = UserService.current_user;
    this.getData();
  }

  getData(){
    this.app.getComponent("loading").show();
    this.dashboardService.list().then((data) => {
      console.log("meumei: Dados do Dashboard obtidos: ");
      console.log(data);
      this.data = data;
      this.app.getComponent("loading").hide();
    });
  }

  openJobCreate(){
    this.nav.push(JobTypePage);
  }

  openJobSearch(){
    this.nav.push(JobSearchPage);
  }
}
