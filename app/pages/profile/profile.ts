import {Page, IonicApp} from 'ionic-angular';
import {UserService} from '../../providers/user-service';

@Page({
  templateUrl: 'build/pages/profile/profile.html'
})
export class ProfilePage {
  user; section;
  constructor(private userService: UserService, private app: IonicApp){
    this.user = UserService.current_user;
    this.section = "profile";
  }

  updateUser(){
    console.log(UserService.current_user);
    UserService.current_user.bio = this.user.bio;
    console.log(UserService.current_user);
    this.app.getComponent("loading").show();
    this.userService.saveUser((user) =>{
      console.log("Biografia atualizada");
      this.app.getComponent("loading").hide();
    });
  }
}
