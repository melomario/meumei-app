import {Page, IonicApp, NavParams, NavController} from 'ionic-angular';
import {UserService} from '../../providers/user-service';
import {JobService} from '../../providers/job-service';

@Page({
  providers: [JobService, UserService],
  templateUrl: 'build/pages/feedback/feedback.html'
})
export class FeedbackPage {
  user; job; mei = {};

  constructor(private app: IonicApp, private nav: NavController, private params: NavParams, private userService: UserService, private jobService: JobService){
    this.job = params.get("job");
    this.loadMei();
  }

  loadMei(){
    this.app.getComponent("loading").show();
    this.userService.get(this.job.responsible_id).then((mei) => {
      this.mei = mei;
      this.app.getComponent("loading").hide();
    });
  }

  updateFeedback(positive){
    if(positive)
      this.job.rating = 1;
    else
      this.job.rating = 0;

    this.app.getComponent("loading").show();
    this.jobService.update(this.job).then((job) => {
      console.log(job);
      this.app.getComponent("loading").hide();
      this.nav.pop();
    });

  }
}
